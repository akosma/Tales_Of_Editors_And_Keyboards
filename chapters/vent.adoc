[[chapter_vent]]
= Le Vent Nous Portera

This short book ends here. If you found interesting what you learnt about Vim and Emacs, then you will most certainly continue the exploration on your own.

The best approach is to continue customizing your configuration files, until the editors look and behave exactly how you want them too. This is the biggest reason to use them; to have them fit your needs and to look the way you want. They are extremely configurable.

Some people have shared their own pre-cooked configuration files. There are countless https://github.com/search?q=dotfiles["dotfiles" repositories] in Github to learn from. You can also install https://github.com/bbatsov/prelude[Prelude for Emacs] or https://github.com/carlhuda/janus[Janus for Vim]. These are just two of a myriad of different precooked environments for both editors, with plenty of settings and plugins ready to be used.

Here is a warning, though. I tried Janus for a while, and then uninstalled it altogether. I found that it made Vim slow to start, and there were many plugins in it that I just did not use. Instead I ended up just installing the things I really used. So my recommendation would be to try Janus or Prelude, and if you like them, then keep them. Otherwise, go as minimalist as possible, and just add the settings you really need. This is what you have done while reading this book.

Also worth mentioning, the http://cedet.sourceforge.net/[CEDET] project, the "Collection of Emacs Development Environment Tools," geared to the creation of a complete development environment in Emacs. I have not gone that far with Emacs yet, but maybe I will some day. If you write a lot of C and C++ code with Emacs, you might want to try kbd:[Meta+X] `gdb` at some point.

But in any case, remember to kbd:[Meta+X] `tetris` once in a while.

Also, you might have noticed that many Unix commands share the shortcuts of Emacs and Vim, which means that you can use them outside of those programs. For example, you can scroll up and down in a `man` page, or the output of the `more` and `less` commands, using kbd:[J] and kbd:[K] or kbd:[Ctrl+N] and kbd:[Ctrl+P]. Even `bash` and `zsh` allow you to kbd:[Ctrl+A] and kbd:[Ctrl+E] to go to the beginning or end of a command before executing it. These calisthenics will prove themselves useful all over Unix.

I cannot finish a book about Emacs without mentioning the venerable https://orgmode.org/[Org Mode], which is touted as one of the greatest organization and creativity tools ever created. Then again, this subject is too much for this short introduction, and it deserves a book of its own. http://www.network-theory.co.uk/org/manual/[Oh, wait.]

If you use Visual Studio Code, you might want to add the https://marketplace.visualstudio.com/items?itemName=vscodevim.vim[Vim] or https://marketplace.visualstudio.com/items?itemName=hiro-sun.vscode-emacs[Emacs] emulation plugins, maybe?

And, if after all these tips and tricks you cannot get enough, try Emacs http://www.ymacs.org/[emulated on a web browser], an actual implementation of Vim https://rhysd.github.io/vim.wasm/[compiled in WebAssembly] and running on your browser, or a https://fanglingsu.github.io/vimb/[web browser] that behaves like Vim. Maybe you could use the latter to view the former, and then dance a little tribal dance in your living room like nobody is watching.

Because, why not? Having fun and learning new things is the true answer to the ultimate question of life, the universe, and everything.

