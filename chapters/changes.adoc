[[chapter_changes]]
= Changes

In 2007 I was finishing my Master degree studies, and I had to write my thesis.

The University of Liverpool, where I was studying, provided us with Microsoft Word templates for the task. After having written two or three pages in it I just could not stand it anymore. The thing would crash for no reason, it was clunky, heavy, and I could not version the files properly. Which meant I had no reliable way to return to a previous version of the document if needed.

I was telling this to https://twitter.com/duciscool[Cedric], a colleague and longtime friend of mine, and he suggested I use LaTeX instead. I had never used it, and barely heard about it, so I gave it a try, and I was hooked from the first moment. It was plain text, which meant I could work with version control tools (back then Git was barely starting to be known, and I stored all of my work in a local Subversion repository.)

But the best thing was that the output produced by LaTeX was gorgeous, including those mathematical equations. I remembered all of the Word documents with Equation Editor thingies inside that I could no longer open; here was a pure text format, oblivious to the test of time, which yielded much more beautiful results. I had an epiphany, and happily wrote the 15'000 words, including code snippets, tables of contents, title page and appendices, in a format that worked incredibly well for me.

With a bit of tweaking, some special templates and help from countless webpages, I got to the point where I would write between 500 to 1000 words every day, no exception, 5 days a week, during a whole month. I could just type text, and I lost nothing. I could undo changes if needed, and I could divide my work in separate files. Each file had its own structure, with source code snippets taken from the original project I was working on, including syntax highlighting. Images were separate files, which I could edit separately if needed, without breaking the flow of the main document.

Even the bibliography was a separate file; a small database of books, papers, web pages and other articles, a small catalog of research, which LaTeX would dutifully read and process automatically for me at every build.

At the end, the table of contents, the table of figures, the mandatory bibliography, and the index would be automatically generated for me. All in all, a PDF file with 140 pages, lots of images and code, all processed and shown in a few seconds.

For this task, my main editor was https://pages.uoregon.edu/koch/texshop/[TeXShop], and I managed the bibliography for my work using https://bibdesk.sourceforge.io/[BibDesk]. All of these tools were included in the magnificent http://www.tug.org/mactex/[MacTeX] distribution, including even more tools in a single package.

And there was this geeky feeling of actually _compiling_ my thesis into a PDF file. I even had a `Makefile` to drive the whole process in as few steps as possible. It was a fitting process for a Master's thesis in software engineering.

The end result looked absolutely stunning, and I got myself a degree with distinction.

Of course, little did I know back then, but both https://github.com/lervag/vimtex[Vim] and https://www.gnu.org/software/emacs/manual/html_node/emacs/TeX-Mode.html[Emacs] have excellent support for LaTeX. I could have written my thesis with them, but TeXShop worked just fine.

