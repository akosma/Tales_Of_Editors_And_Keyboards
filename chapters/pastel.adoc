[[chapter_pastel]]
= Queremos Pastel

Just like using good old scissors on paper, one can cut rectangular portions of text quite easily with these editors.

For Vim, just use kbd:[Ctrl+V] to get into "Visual Block" mode. Then move your cursor, cut and paste. Just hit kbd:[Esc] if this is not what you want.

For Emacs, the sequence is kbd:[Ctrl+X], kbd:[Spacebar]. Move your cursor, and voila. To stop this operation at any time, use the usual kbd:[Ctrl+G] sequence.

