DIR = _build
INPUT = master
OUTPUT = Tales_Of_Editors_And_Keyboards
OUTPUT_FOLDER = --destination-dir=${DIR}
MANPAGE = --backend=manpage
HTML = --backend=html5 --attribute max-width=70em
RAW_HTML = --backend=html5 --attribute stylesheet! --attribute source-highlighter!
PDF =  --backend=pdf --require=asciidoctor-pdf
EPUB = --backend=epub3 --require=asciidoctor-epub3
KINDLE = ${EPUB} --attribute ebook-format=kf8
REMOTE = akosmatr@sl269.web.hostpoint.ch:/home/akosmatr/www/akos.ma/books/Tales_Of_Editors_And_Keyboards

# Public targets

all: man html raw_html pdf epub kindle

html: _build/Tales_Of_Editors_And_Keyboards.html

raw_html: _build/Tales_Of_Editors_And_Keyboards.raw.html

pdf: _build/Tales_Of_Editors_And_Keyboards.pdf

epub: _build/Tales_Of_Editors_And_Keyboards.epub

kindle: _build/Tales_Of_Editors_And_Keyboards.mobi

man: _build/Tales_Of_Editors_And_Keyboards.1

clean:
	if [ -d ".asciidoctor" ]; \
		then rm -r .asciidoctor; \
	fi; \
	if [ -d "${DIR}" ]; \
		then rm -r ${DIR}; \
	fi; \

stats:
	wc -w chapters/*.adoc

zip: man html raw_html pdf epub kindle
	cd ${DIR}; \
	zip -r ${OUTPUT}.zip ${OUTPUT}.1 ${OUTPUT}.pdf ${OUTPUT}.raw.html ${OUTPUT}.html ${OUTPUT}.epub ${OUTPUT}.mobi; \

deploy: zip
	scp ${DIR}/${OUTPUT}.zip ${REMOTE}/${OUTPUT}.zip
	scp ${DIR}/${OUTPUT}.1 ${REMOTE}/${OUTPUT}.1
	scp ${DIR}/${OUTPUT}.pdf ${REMOTE}/${OUTPUT}.pdf
	scp ${DIR}/${OUTPUT}.html ${REMOTE}/${OUTPUT}.html
	scp ${DIR}/${OUTPUT}.raw.html ${REMOTE}/${OUTPUT}.raw.html
	scp ${DIR}/${OUTPUT}.mobi ${REMOTE}/${OUTPUT}.mobi
	scp ${DIR}/${OUTPUT}.epub ${REMOTE}/${OUTPUT}.epub

# Private targets

_build/Tales_Of_Editors_And_Keyboards.1:
	asciidoctor ${MANPAGE} ${OUTPUT_FOLDER} --out-file=${OUTPUT}.1 ${INPUT}.adoc; \

_build/Tales_Of_Editors_And_Keyboards.html:
	asciidoctor ${HTML} ${OUTPUT_FOLDER} --out-file=${OUTPUT}.html ${INPUT}.adoc; \

_build/Tales_Of_Editors_And_Keyboards.raw.html:
	asciidoctor ${RAW_HTML} ${OUTPUT_FOLDER} --out-file=${OUTPUT}.raw.html ${INPUT}.adoc; \

_build/Tales_Of_Editors_And_Keyboards.pdf:
	asciidoctor ${PDF} ${OUTPUT_FOLDER} --out-file=${OUTPUT}.pdf ${INPUT}.adoc; \

_build/Tales_Of_Editors_And_Keyboards.epub:
	asciidoctor ${EPUB} ${OUTPUT_FOLDER} --out-file=${OUTPUT}.epub ${INPUT}.adoc; \

_build/Tales_Of_Editors_And_Keyboards.mobi:
	asciidoctor ${KINDLE} ${OUTPUT_FOLDER} --out-file=${OUTPUT}.mobi ${INPUT}.adoc; \
	if [ -e "${DIR}/${OUTPUT}-kf8.epub" ]; \
		then rm ${DIR}/${OUTPUT}-kf8.epub; \
	fi; \

